import { WidgetProps } from "./types";
import {parseCss} from "../components/Inspector/StyleSelector"
import React, { Component, Fragment, CSSProperties, ChangeEvent } from "react";
import { WidgetDefinition, CommandInputDefinition, StringInputDefinition, StyleInputDefinition, BooleanInputDefinition } from "../types";

import "./styles/BooleanDisplay.styles.css";

type Inputs = {
  title: StringInputDefinition
  onCommand: CommandInputDefinition;
  offCommand: CommandInputDefinition;
  showDevice: BooleanInputDefinition;
  showCommand: BooleanInputDefinition
  onStateArgs: StringInputDefinition;
  offStateArgs: StringInputDefinition;
  onStateImageUrl: StringInputDefinition;
  offStateImageUrl: StringInputDefinition;
  onStateCss: StyleInputDefinition;
  offStateCss: StyleInputDefinition;
  imageCss: StyleInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  checkbox: boolean;
  pending: boolean;
  title: string;
  onStateArgs: string;
  offStateArgs: string;
  showDevice: boolean;
  showCommand: boolean;
  onStateImageUrl: string;
  offStateImageUrl: string;
  onStateCss: any,
  offStateCss: any,
  imageCss: any
}

const style = { padding: "0.5em", whiteSpace: "nowrap" } as CSSProperties;
const styleCheckbox = { padding: "0.5em" } as CSSProperties;
const imageStyle = {
  width: "50px",
  height: "25px"
}
let outerDivCss;

class CommandSwitch extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      pending: false,
      checkbox: false,
      title: this.getAttributeValue('title'),
      showDevice: this.getAttributeValue('showDevice'),
      showCommand: this.getAttributeValue('showCommand'),
      onStateArgs: this.getAttributeValue('onStateArgs'),
      offStateArgs: this.getAttributeValue('offStateArgs'),
      onStateImageUrl: this.getAttributeValue('onStateImageUrl'),
      offStateImageUrl: this.getAttributeValue('offStateImageUrl'),
      onStateCss: parseCss(this.getAttributeValue('onStateCss')).data,
      offStateCss: parseCss(this.getAttributeValue('offStateCss')).data,
      imageCss: parseCss(this.getAttributeValue('imageCss')).data,
    };

    this.checkboxChange = this.checkboxChange.bind(this);
  }

  public render() {
    const { title,showDevice, showCommand } = this.props.inputs;

    outerDivCss = this.state.checkbox === true ? {...style, ...this.state.onStateCss} : {...style, ...this.state.offStateCss};

    let titleLabel = undefined !== title && '' !== title ? title+' ' : '';
    titleLabel += showDevice ? this.showSelectedDeviceOrCommand('device') : '';
    titleLabel += showCommand ? this.showSelectedDeviceOrCommand('command') : '';

    let Checkbox;
    if (false === this.validateUrl(this.state.onStateImageUrl) || false === this.validateUrl(this.state.offStateImageUrl)) {
        Checkbox =
          (
            <span style={styleCheckbox}>
            <label className="toggle">
                <input
                className="attribute-checkbox"
                type="checkbox"
                checked={this.state.checkbox}
                onChange={this.checkboxChange}
                />
                <span className="slider round"></span>
            </label>
            </span>
          );
    } else {
      const imageUrl = true === this.state.checkbox ? this.state.onStateImageUrl : this.state.offStateImageUrl;
      const imageCss = undefined !== this.state.imageCss ? {...imageStyle, ...this.state.imageCss} : imageStyle;

      Checkbox =
        (
          <span style={styleCheckbox}>
          <label className="toggle">
              <input
                  type="checkbox"
                  className="attribute-checkbox"
                  id="attribute-checkbox"
                  checked={this.state.checkbox}
                  onChange={this.checkboxChange}
                  />
              <img alt="not found" src={imageUrl} style={imageCss} />

          </label>
          </span>
      );
    }

    return (
      <div style={outerDivCss}>
        <Fragment>
          {titleLabel} {Checkbox}
        </Fragment>
      </div>
    );
  }

  private getAttributeValue(key): any {

    if(key === 'onStateCss') {
        return this.props.inputs.onStateCss;
    } else if(key === 'offStateCss') {
        return this.props.inputs.offStateCss;
    } else if(key === 'onStateImageUrl') {
        return this.props.inputs.onStateImageUrl;
    } else if(key === 'offStateImageUrl') {
        return this.props.inputs.offStateImageUrl;
    } else if(key === 'onStateArgs') {
        return this.props.inputs.onStateArgs || 'one';
    } else if(key === 'offStateArgs') {
        return this.props.inputs.offStateArgs || 'zero';
    } else if(key === 'imageCss') {
        return this.props.inputs.imageCss;
    } else if(key === 'title') {
        return this.props.inputs.title;
    } else if(key === 'showDevice') {
        return this.props.inputs.showDevice;
    } else if(key === 'showCommand') {
        return this.props.inputs.showCommand;
    }
  }

  /**
   * Returns Device / Command value depending on keyword. If Device & Command are null returns "Device" & "Command"
   *
   * @param keyword: String
   */
  showSelectedDeviceOrCommand(keyword) {
    const { onCommand, offCommand } = this.props.inputs;
    let returnVal = '';

    if ('device' === keyword) {
      returnVal = this.state.checkbox ? null !== onCommand.device ? onCommand.device+'/' : 'Device/' : null !== offCommand.device ? offCommand.device+'/' : 'Device/';
    } else {
      returnVal = this.state.checkbox ? null !== onCommand.command ? onCommand.command : 'Command' : null !== offCommand.command ? offCommand.command : 'Command';
    }

    return returnVal;
  }

  private async checkboxChange(event: ChangeEvent<HTMLInputElement>) {
    if (this.state.pending) {
      return;
    }

    const isChecked = this.state.checkbox;
    const { onCommand, offCommand } = this.props.inputs;
    outerDivCss = this.state.checkbox === true ? {...style, ...this.state.onStateCss} : {...style, ...this.state.offStateCss};
    this.setState({ checkbox: !isChecked, pending: true });

    let inputArray;
    if (this.state.checkbox === false) {
      const acceptedType: string = this.props.inputs.onCommand["acceptedType"];
      const input = this.props.inputs.onStateArgs;

      inputArray = undefined !== acceptedType && acceptedType.includes("Array") ? this.validateInputArray(input, acceptedType) : input;
      onCommand.execute(inputArray);

    } else {
      const acceptedType: string = this.props.inputs.offCommand["acceptedType"];
      const input = this.props.inputs.offStateArgs;

      inputArray = undefined !== acceptedType && acceptedType.includes("Array") ? this.validateInputArray(input, acceptedType) : input;
      offCommand.execute(inputArray);
    }

    this.setState({ pending: false });
  }

  /**
   * Returns array of string if acceptedType is string / var else returns array of float values 
   * 
   * @param input 
   * @param acceptedType 
   */
  private validateInputArray(input, acceptedType: string){
    
    let data = input === undefined ? [] :  input.split(",");

    if(acceptedType.includes("String") || acceptedType.includes("Char")){
      let list: Array<string> = [];
      data.forEach(element => {
        list.push(element);
      })
      return list;

    } else {
      let list: Array<Object> = [];
      data.forEach(element => {
        list.push(Number.parseFloat(element));
      })
      return list;
    }
  }

  private validateUrl(url) {
    return (undefined !== url && -1 !== url.indexOf('http'));
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "Command_Switch",
  name: "Command Switch",
  defaultWidth: 12,
  defaultHeight: 2,

  inputs: {
    title: {
      type:"string",
      label: "Title",
      placeholder: "Title of widget"
    },
    onCommand: {
      label: "On Command",
      type: "command",
      intype: "Any"
    },
    offCommand: {
      label: "Off Command",
      type: "command",
      intype: "Any"
    },
    showDevice: {
      type: "boolean",
      label: "Show Device",
      default: true
    },
    showCommand: {
      type: "boolean",
      label: "Show Command",
      default: true
    },
    onStateArgs: {
        type: "string",
        label: "On State Argument",
        placeholder: "Ex. [1,2,3]"
    },
    offStateArgs: {
        type: "string",
        label: "Off State Argument",
        placeholder: "Ex. [1,2,3]"
    },
    onStateImageUrl: {
        type: "string",
        label: "On State Image Url",
        placeholder: "https://imagelocation?onImage.jpg"
    },
    offStateImageUrl: {
        type: "string",
        label: "Off State Image Url",
        placeholder: "https://imagelocation?offImage.jpg"
    },
    onStateCss: {
        type: "style",
        label: "On State CSS"
    },
    offStateCss: {
        type: "style",
        label: "Off State CSS"
    },
    imageCss: {
      type: "style",
      label: "Image CSS"
    }
  }
};

export default { component: CommandSwitch, definition };
