import React from "react";
import { CommandInput } from "../types";
import CommandSwitch from "./CommandSwitch";

import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

describe('switch-test', () => {
    let myCommandInput: CommandInput;

    it('render switch with normal scenario', () => {
        myCommandInput = {
            device: "sys/tg_test/1",
            command: "configureArray",
            output: "",
            execute: () => null
          };
      
          const element = React.createElement(CommandSwitch.component, {
            mode: "run",
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            inputs: {
              title: 'Master Switch',
              onCommand: myCommandInput,
              offCommand: myCommandInput,
              showDevice: true,
              showCommand: true,
              onStateArgs: '',
              offStateArgs: '',
              onStateImageUrl: '',
              offStateImageUrl: '',
              onStateCss: 'background-color:green',
              offStateCss: 'background-color:red',
              imageCss: ''
            }
          });

          const elemNoWhiteSpace = shallow(element).html().replace(/\s/g, '');
          expect(elemNoWhiteSpace).toContain("sys/tg_test/1/configureArray");
          expect(elemNoWhiteSpace).toContain("MasterSwitch");
    });

    it('render switch with custom image', () => {
        myCommandInput = {
            device: "sys/tg_test/1",
            command: "configureArray",
            output: "",
            execute: () => null
          };
      
          const element = React.createElement(CommandSwitch.component, {
            mode: "run",
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            inputs: {
              title: 'Master Switch',
              onCommand: myCommandInput,
              offCommand: myCommandInput,
              showDevice: true,
              showCommand: true,
              onStateArgs: '',
              offStateArgs: '',
              onStateImageUrl: 'https://picsum.photos/200/300',
              offStateImageUrl: 'https://picsum.photos/200/300',
              onStateCss: 'background-color:green',
              offStateCss: 'background-color:red',
              imageCss: 'border-radius:50%'
            }
          });

          const elemNoWhiteSpace = shallow(element).html().replace(/\s/g, '');
          expect(elemNoWhiteSpace).toContain("sys/tg_test/1/configureArray");
          expect(elemNoWhiteSpace).toContain("border-radius:50%");
        });

    it('check click event of switch', () => {
        myCommandInput = {
            device: "sys/tg_test/1",
            command: "DevString",
            output: "",
            execute: () => null
        };

        const element = React.createElement(CommandSwitch.component, {
            mode: "run",
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            inputs: {
                title: 'Master Switch',
                onCommand: myCommandInput,
                offCommand: myCommandInput,
                showDevice: true,
                showCommand: true,
                onStateArgs: '',
                offStateArgs: '',
                onStateImageUrl: 'https://picsum.photos/200/300',
                offStateImageUrl: 'https://www.dumpaday.com/wp-content/uploads/2020/06/00-100-750x280.jpg',
                onStateCss: 'background-color:green',
                offStateCss: 'background-color:red',
                imageCss: 'border-radius:50%'
            }
        });

        //Trigger the click event on checkbox & confirm if image changes on switch
        const shallowElement = shallow(element);
        shallowElement.find('.attribute-checkbox').simulate('change');
        const checkboxState = shallowElement.state()['checkbox'];
        const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, '');

        if (checkboxState) {
            expect(elemNoWhiteSpace).toContain("background-color:green");
            expect(elemNoWhiteSpace).toContain("https://picsum.photos/200/300");
          } else {
            expect(elemNoWhiteSpace).toContain("background-color:red");
            expect(elemNoWhiteSpace).toContain("https://www.dumpaday.com/wp-content/uploads/2020/06/00-100-750x280.jpg");
        }

        expect(elemNoWhiteSpace).toContain("sys/tg_test/1/DevString");
        expect(elemNoWhiteSpace).toContain("border-radius:50%");
    });

});
