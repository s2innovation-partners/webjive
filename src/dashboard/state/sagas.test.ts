import sagas, { importDashboardSaga } from "./sagas";
import { call } from "redux-saga/effects";
import { exportDashboardSaga, loadDashboardSaga } from './sagas';

import { runSaga, stdChannel } from "redux-saga";
import * as API from "../dashboardRepo";
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import dashboard from './reducers/dashboards'

import { deleteWidget, duplicateWidget, resizeWidget, loadDashboard,exportDashboard, importDashboard, saveDashboard} from "./actionCreators";

const example_dashboard = {
  id: '1',
  name: "",
  user: "",
  insertTime: "",
  updateTime: "",
  group: "",
  groupWriteAccess: "",
  lastUpdatedBy: "",
  widgets: [
    {
      "_id": "5ef60bf206f0f400173d387e",
      "id": "1",
      "x": 9,
      "y": 8,
      "canvas": "0",
      "width": 10,
      "height": 2,
      "type": "ATTRIBUTE_DISPLAY",
      "inputs": {
        "attribute": {
          "device": "sys/tg_test/1",
          "attribute": "double_scalar",
          "label": "double_scalar"
        },
        "precision": 2,
        "showDevice": false,
        "showAttribute": "Label",
        "scientificNotation": false,
        "showEnumLabels": false,
        "textColor": "#000000",
        "backgroundColor": "#ffffff",
        "size": 1,
        "font": "Helvetica"
      },
      "order": 0
    }
  ]
};

const example_widgets =  [
  {
    "_id": "5ef60bf206f0f400173d387e",
    "id": "1",
    "x": 9,
    "y": 8,
    "canvas": "0",
    "width": 10,
    "height": 2,
    "type": "ATTRIBUTE_DISPLAY",
    "inputs": {
      "attribute": {
        "device": "sys/tg_test/1",
        "attribute": "double_scalar",
        "label": "double_scalar"
      },
      "precision": 2,
      "showDevice": false,
      "showAttribute": "Label",
      "scientificNotation": false,
      "showEnumLabels": false,
      "textColor": "#000000",
      "backgroundColor": "#ffffff",
      "size": 1,
      "font": "Helvetica"
    },
    "order": 0
  }
];

const example_dashboardR = 
{ 
  id: undefined,
  name: '',
  user: '',
  insertTime: '',
  updateTime: '',
  group: '',
  groupWriteAccess: '',
  lastUpdatedBy: '' 
}

const example_notification = { 
  level: 'ERROR',
  action: 'EXPORT_DASHBOARD',
  msg: 'Dashboard not exported: Error: error',
  duration: 2000 
}

const importNotification = { 
  level: 'ERROR',
  action: 'IMPORT_DASHBOARD',
  msg: 'Dashboard not imported: [object Object]',
  duration: 2000 
}

//Integration testing

it('handles reducers and store state', () => {

  const mockGetUsers = jest.spyOn(API, 'load');
  mockGetUsers.mockResolvedValue(example_dashboard);

  return expectSaga(loadDashboardSaga)
    .dispatch(
      { type: 'LOAD_DASHBOARD', payload: {id:1} }
    )
    .withReducer(dashboard)

    .hasFinalState({
      dashboards: []
    })

    .run();
});

it('Assert DASHBOARD_EXPORTED', async () => {

  const mockGetUsers = jest.spyOn(API, 'exportDash');
  mockGetUsers.mockResolvedValue(example_dashboard);
 
  return expectSaga(exportDashboardSaga)
    .provide([
      [
        call(API.exportDash, 1), 
        example_dashboard,
      ]
    ])
    // Assert that the `put` will eventually happen.
    .put({
      type: 'DASHBOARD_EXPORTED',
      dashboard: example_dashboardR,
      widgets: example_widgets,
    })
    // Dispatch any actions that the saga will `take`.
    .dispatch(
      { type: 'EXPORT_DASHBOARD', payload: 1 }
    )
    // Start the test. Returns a Promise.
    .run();
});

it('Assert DASHBOARD_IMPORTED', async () => {

  const mockGetUsers = jest.spyOn(API, 'importDash');
  mockGetUsers.mockResolvedValue(example_dashboard);
 
  return expectSaga(importDashboardSaga)
    .provide([
      [
        call(API.importDash), 
        example_dashboard,
      ]
    ])
    // Assert that the `put` will eventually happen.
    .put({
      type: 'SHOW_NOTIFICATION',
      notification: importNotification,
    })
    // Dispatch any actions that the saga will `take`.
    .dispatch(
      { type: 'IMPORT_DASHBOARD', payload: example_dashboard }
    )
    // Start the test. Returns a Promise.
    .run();
});

it('Show NOTIFICATION on error', () => {

  const mockGetUsers = jest.spyOn(API, 'exportDash');
  mockGetUsers.mockResolvedValue(example_dashboard);

  const error = new Error('error');

  return expectSaga(exportDashboardSaga)
    .provide([
      [matchers.call.fn(API.exportDash), throwError(error)],
    ])
    .put({ type: 'SHOW_NOTIFICATION', notification: example_notification })
    .dispatch({ type: 'EXPORT_DASHBOARD', payload: 1 })
    .run();
});

//Unit testing

test('exportDashboard SAGA Flow', async () => {
  const exportDashboardSagav = exportDashboardSaga();
  const takeEffect = exportDashboardSagav.next();
  expect(takeEffect.value.type).toEqual("TAKE");
  expect(takeEffect.value.payload.pattern).toEqual("EXPORT_DASHBOARD");

  const basicW = {
    id: "1",
    name: "",
    user: "",
    insertTime: "",
    updateTime: "",
    group: "",
    groupWriteAccess: "",
    lastUpdatedBy: "",
    widgets: {}
  };

  const callEffect = exportDashboardSagav.next(basicW).value;

  expect(callEffect.type).toEqual("CALL");
  expect(callEffect).toEqual(call(API.exportDash,'1'));

  const dashboard = {
    id: "1",
    name: "",
    user: "",
    insertTime: "",
    updateTime: "",
    group: "",
    groupWriteAccess: "",
    lastUpdatedBy: "",
  };
  const widgets = [];

  const putEffect = exportDashboardSagav.next(dashboard,widgets);

  expect(putEffect.value.type).toEqual("PUT");

});

test('importDashboard SAGA Flow', async () => {
  const importDashboardSagav = importDashboardSaga();
  const takeEffect = importDashboardSagav.next();
  expect(takeEffect.value.type).toEqual("TAKE");
  expect(takeEffect.value.payload.pattern).toEqual("IMPORT_DASHBOARD");

  const basicW = {
    id: "1",
    name: "",
    user: "",
    insertTime: "",
    updateTime: "",
    group: "",
    groupWriteAccess: "",
    lastUpdatedBy: "",
    widgets: {}
  };

  const callEffect = importDashboardSagav.next(basicW).value;

  expect(callEffect.type).toEqual("CALL");
  expect(callEffect).toEqual(call(API.importDash,undefined));

  const putEffect = importDashboardSagav.next();

  expect(putEffect.value.type).toEqual("PUT");

});

const basicState = {
  widgets: {},
  selectedIds: [],
  id: "123",
  name: "",
  user: "",
  group: "",
  lastUpdatedBy: "",
  redirect: false,
  insertTime: null,
  updateTime: null,
  history: {
    undoActions: [],
    redoActions: [],
    undoIndex: 0,
    redoIndex: 0,
    undoLength: 0,
    redoLength: 0,
  }
};

test("DELETE_WIDGETS", () => {
  const ids = ["1", "2", "3", "4", "5"];
  const widgets = ids.reduce((accum, id) => {
    return {
      ...accum,
      [id]: {
        id,
        type: "",
        valid: true,
        x: 0,
        y: 0,
        canvas: "",
        width: 0,
        height: 0,
        inputs: []
      }
    };
  }, {});

  const withSelection = {
    ...basicState,
    widgets,
    selectedIds: ["1", "2", "3"]
  };
  const dpActions = [];
  const channel = stdChannel();
  const fakeStore = {
    dispatch: (action) => dpActions.push(action),
    getState: () => ({ selectedDashboard: withSelection }),
    channel
  };
  runSaga(fakeStore, sagas);
  channel.put(deleteWidget());
  channel.close();
  const dashboardEditedAction = dpActions.find(function(a) {
    return a.type == "DASHBOARD_EDITED"
  });
  const idsAfter = Object.keys(dashboardEditedAction.dashboard.widgets);

  expect(idsAfter).toEqual(["4", "5"]);
  expect(dashboardEditedAction.dashboard.selectedIds).toEqual([]);
});

test("DUPLICATE_WIDGETS", () => {
  const ids = ["1", "2", "3", "4", "5"];
  const widgets = ids.reduce((accum, id) => {
    return {
      ...accum,
      [id]: {
        id,
        type: "",
        valid: true,
        x: 0,
        y: 0,
        canvas: "",
        width: 0,
        height: 0,
        inputs: []
      }
    };
  }, {});

  const withSelection = {
    ...basicState,
    widgets,
    selectedIds: ["1", "2", "3"]
  };
  const dpActions = [];
  const channel = stdChannel();
  const fakeStore = {
    dispatch: (action) => dpActions.push(action),
    getState: () => ({ selectedDashboard: withSelection }),
    channel
  };
  runSaga(fakeStore, sagas);
  channel.put(duplicateWidget());
  channel.close();
  const dashboardEditedAction = dpActions.find(function(a) {
    return a.type == "DASHBOARD_EDITED"
  });
  const idsAfter = Object.keys(dashboardEditedAction.dashboard.widgets);

  expect(idsAfter).toEqual(["1", "2", "3", "4", "5", "6", "7", "8"]);
});

test("RESIZE_WIDGETS", () => {
  const ids = ["1", "2", "3", "4", "5"];
  const widgets = ids.reduce((accum, id) => {
    return {
      ...accum,
      [id]: {
        id,
        type: "",
        valid: true,
        x: 0,
        y: 0,
        canvas: "",
        width: 0,
        height: 0,
        inputs: []
      }
    };
  }, {});

  const withSelection = {
    ...basicState,
    widgets,
    selectedIds: ["1"]
  };
  const dpActions = [];
  const channel = stdChannel();
  const fakeStore = {
    dispatch: (action) => dpActions.push(action),
    getState: () => ({ selectedDashboard: withSelection }),
    channel
  };
  runSaga(fakeStore, sagas);
  channel.put(resizeWidget("1",10,20,30,40));
  channel.close();
  const dashboardEditedAction = dpActions.find(function(a) {
    return a.type == "DASHBOARD_EDITED"
  });

  const {x,y,width,height} = dashboardEditedAction.dashboard.widgets['1'];

  expect(x).toEqual(10);
  expect(y).toEqual(20);
  expect(width).toEqual(30);
  expect(height).toEqual(40);
});

test("LOAD_DASHBOARD", () => {
  const dpActions = [];
  const channel = stdChannel();
  const fakeStore = {
    dispatch: (action) => dpActions.push(action),
    getState: () => ({ Dashboard: basicState }),
    channel
  };
  runSaga(fakeStore, sagas);
  fakeStore.dispatch(loadDashboard("1"))
  channel.put(loadDashboard("1"));
  channel.close();
  //console.log(dpActions);

  expect(dpActions[0].type).toEqual("LOAD_DASHBOARD");
  
});

test("EXPORT_DASHBOARD", () => {

  const dpActions = [];
  const channel = stdChannel();
  const fakeStore = {
    dispatch: (action) => dpActions.push(action),
    getState: () => ({ id: "1" }),
    channel
  };
  runSaga(fakeStore, sagas);
  fakeStore.dispatch(exportDashboard("1"))
  channel.put(exportDashboard("1"));
  channel.close();
  expect(dpActions[0].type).toEqual("EXPORT_DASHBOARD");
});



