import React from 'react'
import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import DashboardLibrary from './DashboardLibrary'

import { Provider } from "react-redux";

import store from "../../state/store";
import dashboard from '../../state/reducers/dashboards';


configure({ adapter: new Adapter() });


describe("Import dashboard from file", () => {

    it("button renders correctly", () => {

        const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
            isLoggedIn: true, 
            render: true, 
            dashboards: [], 
            selectedDashboard: []
          });
        
          expect(mount(<Provider store={store}>{elementHtml}</Provider>).html()).toContain("dashboard-menu-button");
    });

    it("hide button for not logged user", () => {

        const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
            isLoggedIn: false, 
            render: true, 
            dashboards: [], 
            selectedDashboard: []
          });
        
        expect(mount(<Provider store={store}>{elementHtml}</Provider>).html()).not.toContain("dashboard-menu-button");
        expect(mount(<Provider store={store}>{elementHtml}</Provider>).html()).toContain("You have to be logged in to view and manage your dashboards.");
        
    });

});
