import React, { Component, Fragment } from "react";

import {
    InputDefinitionMapping,
    InputMapping,
    IndexPath,
    Widget,
    WidgetBundle,
} from "../../types";

import NumericInput from "./NumericInput";
import AttributeSelect from "./AttributeSelect";
import DeviceSelect from "./DeviceSelect";
import CommandSelect from "./CommandSelect";
import StyleSelector from "./StyleSelector";
import { Button, Alert } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle, faCalendarPlus } from "@fortawesome/free-solid-svg-icons";

interface Props {
    tangoDB: string;
    inputDefinitions: InputDefinitionMapping;
    inputs: InputMapping;
    widgets: Widget[];
    onChange: (path: IndexPath, value) => void;
    onAdd: (path: IndexPath) => void;
    onDelete: (path: IndexPath) => void;
    widgetType?: string;
    basePath?: IndexPath;
    nonEditable: boolean;
}

export default class InputList extends Component<Props> {
    public render() {
        const {
            inputDefinitions,
            inputs,
            widgets,
            tangoDB,
            widgetType,
            nonEditable
        } = this.props;

        const inputNames = Object.keys(inputDefinitions);

        const hasSameValue = inputNames.reduce((accum, inputName) => {
            const result = widgets.reduce(
                (acc, curr) =>
                    acc &&
                    curr.inputs[inputName] === widgets[0].inputs[inputName],
                true
            );
            return { ...accum, [inputName]: result };
        }, {});
        const inputControls = inputNames.map((inputName) => {
            const inputDefinition = inputDefinitions[inputName];
            const definitionLabel = inputDefinition.label;
            const warningSign =
                !hasSameValue[inputName] && definitionLabel !== "" ? (
                    <b style={{ transition: "opacity 0.1s", color: "red" }}>
                        *
                    </b>
                ) : (
                    ""
                );
            const label =
                definitionLabel == null ? (
                    inputName
                ) : (
                    <p
                        style={{ marginBottom: 0 }}
                        title={
                            hasSameValue[inputName]
                                ? ""
                                : "Selected widgets have different values in this field"
                        }
                    >
                        {definitionLabel}
                        {warningSign}
                    </p>
                );

            if (inputDefinition.type === "number") {
                const value = inputs[inputName] as number;
                return (
                    <tr key={inputName}>
                        <td>{label}</td>
                        <td>
                            <NumericInput
                                className="form-control"
                                value={value}
                                onChange={(inputValue) =>
                                    this.props.onChange([inputName], inputValue)
                                }
                            />
                        </td>
                    </tr>
                );
            } else if (inputDefinition.type === "boolean") {
                const value = inputs[inputName] as boolean;
                return (
                    <tr key={inputName}>
                        <td>{label}</td>
                        <td>
                            <input
                                type="checkbox"
                                checked={value}
                                onChange={(e) =>
                                    this.props.onChange(
                                        [inputName],
                                        e.target.checked
                                    )
                                }
                            />
                        </td>
                    </tr>
                );
            } else if (inputDefinition.type === "style") {
                const value = inputs[inputName] as string;
                return (
                    <React.Fragment key={inputName}>
                        <tr>
                            <td colSpan={2}>
                                <StyleSelector
                                    label={label}
                                    value={value}
                                    onChange={(parsedCss: string) =>
                                        this.props.onChange(
                                            [inputName],
                                            parsedCss
                                        )
                                    }
                                />
                            </td>
                        </tr>
                    </React.Fragment>
                );
            } else if (inputDefinition.type === "string") {
                const value = inputs[inputName] as string;
                return (
                    <tr key={inputName}>
                        <td>{label}</td>
                        <td>
                            <input
                                className="form-control"
                                type="text"
                                value={value}
                                placeholder={inputDefinition.placeholder || ""}
                                onChange={(e) =>
                                    this.props.onChange(
                                        [inputName],
                                        e.target.value
                                    )
                                }
                            />
                        </td>
                    </tr>
                );
            } else if (inputDefinition.type === "color") {
                const value = inputs[inputName] as string;
                return (
                    <tr key={inputName}>
                        <td>{label}</td>
                        <td>
                            <input
                                type="color"
                                value={value}
                                onChange={(e) =>
                                    this.props.onChange(
                                        [inputName],
                                        e.target.value
                                    )
                                }
                            />
                        </td>
                    </tr>
                );
            } else if (inputDefinition.type === "attribute") {
                const constantDevice = inputDefinition.device != null;
                const constantAttribute = inputDefinition.attribute != null;

                if (constantDevice && constantAttribute) {
                    return null;
                }

                const value = inputs[inputName] as {
                    device: string;
                    attribute: string;
                    label: string;
                };

                if (constantDevice) {
                    return (
                        <tr key={inputName}>
                            <td colSpan={2}>
                                {label}
                                <input
                                    type="text"
                                    className="form-control"
                                    value={value.label}
                                    onChange={(event) =>
                                        this.props.onChange([inputName], {
                                            device: null,
                                            attribute: event.target.value,
                                        })
                                    }
                                />
                            </td>
                        </tr>
                    );
                } else {
                    return (
                        <tr key={inputName}>
                            <td colSpan={2}>
                                {label}
                                <AttributeSelect
                                    nonEditable={nonEditable}
                                    tangoDB={tangoDB}
                                    device={value.device}
                                    attribute={value.attribute}
                                    label={value.label}
                                    dataFormat={inputDefinition.dataFormat}
                                    dataType={inputDefinition.dataType}
                                    onSelect={(device, attribute, label) =>
                                        this.props.onChange([inputName], {
                                            device,
                                            attribute,
                                            label
                                        })
                                    }
                                />
                            </td>
                        </tr>
                    );
                }
            } else if (inputDefinition.type === "device") {
                const value = inputs[inputName] as string;
                return (
                    <tr key={inputName}>
                        <td>{label}</td>
                        <td>
                            <DeviceSelect
                                nonEditable={nonEditable}
                                tangoDB={tangoDB}
                                device={value}
                                onSelect={(device) =>
                                    this.props.onChange([inputName], device)
                                }
                            />
                        </td>
                    </tr>
                );
            } else if (
                inputDefinition.type === "complex" &&
                inputDefinition.repeat === true
            ) {
                const value = inputs[inputName] as InputMapping[];
                return (
                    <Fragment key={inputName}>
                        <tr>
                            <td>{label}</td>
                            <td>
                                <button
                                    className="btn btn-outline-dark"
                                    type="button"
                                    onClick={() => {
                                        // Doesn't support more than one degree of nesting
                                        this.props.onAdd([inputName]);
                                    }}
                                >
                                    <span className="fa fa-plus" />
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan={2}>
                                {value.map((each, j) => (
                                    <div className="ComplexInput" key={j}>
                                        <Button
                                            style={{
                                                position: "absolute",
                                                right: "0em",
                                                top: "0em",
                                                padding: 0,
                                                zIndex: 2,
                                                boxShadow: "none",
                                            }}
                                            onClick={() =>
                                                this.props.onDelete([
                                                    inputName,
                                                    j,
                                                ])
                                            }
                                        >
                                            <FontAwesomeIcon
                                                icon={faTimesCircle}
                                            ></FontAwesomeIcon>
                                        </Button>
                                        <InputList
                                            tangoDB={tangoDB}
                                            inputDefinitions={
                                                inputDefinition.inputs
                                            }
                                            inputs={each}
                                            widgets={[]}
                                            onChange={(path2, value2) => {
                                                this.props.onChange(
                                                    [inputName, j, ...path2],
                                                    value2
                                                );
                                            }}
                                            onDelete={(path2) =>
                                                this.props.onDelete([
                                                    inputName,
                                                    j,
                                                    ...path2,
                                                ])
                                            }
                                            nonEditable={nonEditable}
                                            onAdd={(path) => null /* ??? */}
                                        />
                                    </div>
                                ))}
                            </td>
                        </tr>
                    </Fragment>
                );
            } else if (inputDefinition.type === "select") {
                const value = inputs[inputName] as string[];
                return (
                    <tr key={inputName}>
                        <td>{label}</td>
                        <td>
                            <select
                                className="form-control"
                                value={value}
                                onChange={(e) =>
                                    this.props.onChange(
                                        [inputName],
                                        e.currentTarget.value
                                    )
                                }
                            >
                                {inputDefinition.options.map((option, j) => (
                                    <option key={j} value={option.value}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </td>
                    </tr>
                );
            } else if (inputDefinition.type === "command") {
                const value = inputs[inputName] as {
                    device: string;
                    command: string;
                    acceptedType: string;
                };


                const constantDevice = inputDefinition.device != null;
                const constantCommand = inputDefinition.command != null;

                if (constantDevice && constantCommand) {
                    return null;
                }


                if (constantDevice) {
                    return (
                        <tr key={inputName}>
                            <td>{label}</td>
                            <td>
                                <input
                                    type="text"
                                    value={value.command}
                                    onChange={(e) =>
                                        this.props.onChange([inputName], {
                                            device: null,
                                            command: e.target.value,
                                            acceptedType: e.target.value,
                                        })
                                    }
                                />
                            </td>
                        </tr>
                    );
                } else {
                    return (
                        <tr key={inputName}>
                            <td colSpan={2}>
                                {label}
                                <CommandSelect
                                    nonEditable={nonEditable}
                                    tangoDB={tangoDB}
                                    device={value.device}
                                    command={value.command}
                                    inputType={inputDefinition.intype}
                                    selectMultipleCommands={
                                        widgetType === "CONTAINER_FOR_DEVICE"
                                    }
                                    onSelect={(device, command, acceptedType) =>
                                        this.props.onChange([inputName], {
                                            device,
                                            command, 
                                            acceptedType                             
                                         })
                                    }
                                />
                            </td>
                        </tr>
                    );
                }
            }

            return (
                <tr key={inputName}>
                    <td colSpan={2}>
                        {label}
                        <pre>{JSON.stringify(inputDefinition)}</pre>
                    </td>
                </tr>
            );
        });

        const hasInputs = !!inputControls.find((control) => control !== null);
        const inner = hasInputs ? (
            inputControls
        ) : (
            <div>There are no configurable inputs for this widget.</div>
        );

        return (
            <div>
                <table style={{ width: "100%" }}>
                    <tbody>{inner}</tbody>
                </table>
                {widgets.length > 1 ? (
                    <div>
                        <Alert variant="info" style={{ padding: "6px 20px" }}>
                            {widgets.length} widgets are selected
                        </Alert>
                    </div>
                ) : null}
            </div>
        );
    }
}
